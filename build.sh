#!/bin/bash
set -xe

[ -d build ] || git clone https://gitlab.com/ubports/community-ports/halium-generic-adaptation-build-tools build

# Cosmo Communicator specific hack, unpack deb packages needed for CoDi daemon to overlay
./codi-overlay.sh

./build/build.sh "$@"
